import React, { Component } from 'react';

class TextInput extends Component {
    constructor(props){
        super(props);
        this.state = {
            'value' : props.value,
            'label' : props.label,
            'name'  : props.name,
        };
    }

    changeCallback(e) {
        this.setState({'value': e.target.value}, () => {
            if(this.props.callback !== null) {
                this.props.callback(this.state.value, this.state.name);
            }
        });
    }

    render(){
        return <div className="text-input-wrapper">
            <label htmlFor={this.state.name} className="text-input-label">
                {this.state.label}
            </label>
            <input type="text"
               name={this.state.name}
               value={this.state.value}
               onChange={e => this.changeCallback(e)}
            />
        </div>
    }
}

export default TextInput;