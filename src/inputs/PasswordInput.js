import React, { Component } from 'react';

class PasswordInput extends Component {
    constructor(props){
        super(props);
        this.state = {
            'value' : props.value,
            'label' : props.label,
            'name'  : props.name,
        };
    }

    changeCallback(e) {
        this.setState({'value': e.target.value}, () => {
            if(this.props.callback !== null) {
                this.props.callback(this.state.value, this.state.name);
            }
        });
    }

    render(){
        return <div className="password-input-wrapper">
            <label htmlFor={this.state.name} className="password-input-label">
                {this.state.label}
            </label>
            <input type="password"
                   name={this.state.name}
                   value={this.state.value}
                   onChange={e => this.changeCallback(e)}
            />
        </div>
    }
}

export default PasswordInput;