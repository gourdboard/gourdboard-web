import React, { Component } from 'react';

class PasswordRegister extends Component {
    constructor(props){
        super(props);
        this.state = {
            password : {
                value : props.passValue,
                label : props.passLabel,
                name  : props.passName,
            },
            confirmation : {
                value : props.confirmValue,
                label : props.confirmLabel,
                name  : props.confirmName,
            },
            length : props.passwordLength,
            symbolRequired : props.symbolRequired,
            numberRequired : props.numberRequired,
            dirty : false
        };
    }

    lengthCheck() {
        return this.state.password.value.length >= this.state.length;
    }

    passwordsMatch() {
        return this.state.password.value === this.state.confirmation.value;
    }

    symbolCheck() {
        let symbolRegex = /[!@#$%^&*()~]+/g;
        return this.state.symbolRequired && symbolRegex.test(this.state.password.value);
    }

    numberCheck() {
        let numberRegex = /[0-9]+/g;
        return this.state.numberRequired && numberRegex.test(this.state.password.value);
    }

    setPasswordValue(e, stateKey) {
        if (this.state.dirty === false) {
            this.setState({'dirty': true});
        }

        let password = this.state[stateKey];
        password.value = e.target.value;
        if (password.value.length === 0) {
            this.setState({'dirty': false});
        }
        this.setState({ [stateKey]: password });
        
    }

    render(){
        return <div className="password-registration-wrapper">
            <div className="password-input">
                <label htmlFor={this.state.password.name} className="password-input-label">
                    {this.state.password.label}
                </label>
                <input type="password"
                   name={this.state.password.name}
                   value={this.state.password.value}
                   onChange={e => this.setPasswordValue(e, 'password')}
                />
            </div>
            <div className="password-confirmation-input">
                <label htmlFor={this.state.confirmation.value} className="password-confirmation-label">
                    {this.state.confirmation.label}
                </label>
                <input type="password"
                   name={this.state.confirmation.name}
                   value={this.state.confirmation.value}
                   onChange={e => this.setPasswordValue(e, 'confirmation')}
                />
            </div>
            {this.state.dirty
                ? <div className="password-register-success">
                    <p>Symbol Required : {this.state.symbolRequired ? "True" : "False"}</p>
                    <p>Symbol In Pass : {this.symbolCheck() ? "True" : "False"}</p>
                    <p>Number Required : {this.state.symbolRequired ? "True" : "False"}</p>
                    <p>Number in Pass : {this.numberCheck() ? "True" : "False"}</p>
                    <p>Length required : {this.state.length}</p>
                    <p>Length achieved : {this.lengthCheck() ? "Pass" : "Fail"}</p>
                    <p>Passwords Match : {this.passwordsMatch() ? "Pass" : "Fail"}</p>
                </div>
                : ""
            }
        </div>
    }
}

export default PasswordRegister;