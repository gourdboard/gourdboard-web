import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import Authenticator from './auth/auth';
import TextInput from './inputs/TextInput';
import PasswordInput from './inputs/PasswordInput';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            identifier: "",
            password: "",
            errorMessage: "",
        }
    }

    updateState(value, keyName = null) {
        this.setState({[keyName]: value})
    }

    attemptLogin() {
        this.setState({message: null});
        Authenticator.loginPromise(this.state.identifier, this.state.PasswordInput)
            .then(resp =>{
                if(resp.data.success === true) {
                    Authenticator.setToken(resp.data.api_token);

                } else {
                    Authenticator.removeToken();
                    this.setState({message: 'Incorrect username or password'});
                }
            })
            .catch(err => {
                console.error(err);
                Authenticator.removeToken();
                if(err.response.status === 422) {
                    this.setState({message: "Incorrect username or password"});
                }
            });
    }

    render(){
        return Authenticator.isLoggedIn()
            ? <Redirect to="/protected" />
            : <header>
                <h1>GourdBoard</h1>

                <div className="form">
                    <TextInput name="identifier" label="Username or email address"
                        callback={(val, key) => this.updateState(val, key)}
                    />
                    <PasswordInput name="password" label="Password"
                       callback={(val, key) => this.updateState(val,key)}
                    />
                    { this.state.message !== null
                        ? <p className="error">{this.state.message}</p>
                        : null
                    }
                    <button onClick={() => this.attemptLogin()}>
                        Log In
                    </button>
                </div>
            </header>;
    }
}

export default Login;