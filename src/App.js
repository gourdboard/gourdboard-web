import React, { Component } from 'react';
import {BrowserRouter as Router, Route, Link } from "react-router-dom";
import Login from './Login';
import Register from './Register'
import PrivateRoute from "./auth/PrivateRoute";
import Authenticator from "./auth/auth";

class ProtectedComponent extends Component {
    render() {
        return (
            <h1>Protected!</h1>
        );
    }
}

class App extends Component {
    linksTemplate(){
        return Authenticator.isLoggedIn()
            ?  <div className="links">
                <Link to="/logout">Logout</Link>
                <Link to="/protected">Protected</Link>
            </div>
            : <div className="links">
                <Link to="/login">Login</Link>
                <Link to="/register">Register</Link>
                <Link to="/protected">Protected</Link>
            </div>
    }

    render() {
        let routes, links = null;
        links = this.linksTemplate();

        routes = <div className="routes">
            <Route path="/login" render={() => <Login />} />
            <Route path="/register" component={Register} />
            <PrivateRoute path="/protected" component={ProtectedComponent} />
        </div>;

        return (
            <Router className="App">
                {links}
                {routes}
            </Router>
        );
    }
}

export default App;
