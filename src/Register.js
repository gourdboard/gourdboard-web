import React, { Component } from 'react';
import EmailInput from './inputs/EmailInput';
import TextInput from './inputs/TextInput';
import PasswordRegister from './inputs/PasswordRegister';

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            email: "",
            password: "",
            password_confirmation: "",
            form_valid : false
        }
    }

    updateState(val, key) {
        this.setState({[key]: val});
    }

    checkFormIsValid() {
        let valid =  this.state.username.length > 0 &&
            this.state.email.length > 0 &&
            this.state.password.length > 0 &&
            this.state.password === this.state.password_confirmation;
        console.log("Checking again", valid);
        this.setState({form_valid: valid});
    }

    render() {
        return <header>
            <h1>GourdBoard</h1>

            <div className="form">
                <TextInput name="username" label="Username"
                   callback={(val, key) => this.updateState(val, key)} />
                <EmailInput name="email" label="Email"
                    callback={(val, key) => this.updateState(val, key)} />

                <PasswordRegister
                    passName="password"
                    passLabel="Password"
                    confirmName="password_confirmation"
                    confirmLabel="Password Confirmation"
                    symbolRequired={true}
                    numberRequired={true}
                    passwordLength={6}
                    passValue=""
                    callback={() => this.checkFormIsValid()}
                />
                
                <button onClick={this.attemptLogin} disabled={!this.state.form_valid}>
                    Log In
                </button>
            </div>
        </header>;
    }
}

export default Register;