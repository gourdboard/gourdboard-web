import api from "./api";
import debug from '../util/debugger';

export default class Authenticator{

    static loginPromise(identifier, password){
        debug(`Hey we trying to log in with ${identifier}:${password}`);
        let requestBody = {
            identifier: identifier,
            password: password,
        };
        return api.post("/auth/authenticate", requestBody);
    }

    static login(identifier, password) {
        this.loginPromise(identifier, password)
            .then(response => {
                if(response.data.success === true) {
                    Authenticator.setToken(response.data.api_token);
                } else {
                    Authenticator.removeToken()
                }
            })
            .catch(err => {
                Authenticator.removeToken()
            });
    }

    static setToken(token){
        debug("set token: ", token);
        localStorage.setItem("gbToken", token);
    }

    static removeToken(){
        debug("remove token");
        localStorage.removeItem("gbToken");
    }

    static isLoggedIn(){
        return localStorage.getItem("gbToken" ) !== null;
    }

    static isNotLoggedIn(){
        return ! Authenticator.isLoggedIn();
    }
}