import React from 'react';
import {Redirect, Route} from "react-router-dom";
import Authenticator from './auth.js';

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props =>
        Authenticator.isLoggedIn() === true
            ? (<Component {...props} />)
            : (<Redirect to={{
                    pathname: "/login",
                    state: { from: props.location }
                }} />
            )
    }/>
);

export default PrivateRoute;
