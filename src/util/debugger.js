export default function debug(message, override=false, method=console.log){
    if(process.env.REACT_APP_DEBUG && !override){
        method(message);
    }
}